# cypress-cucumber-e2e-testing

> Cypress 10+ SeamlessHr QA Assesment project.

### 💻 Topics


- ## 💻 Steps to Execute

Before you use this project you only need to have Node Js installed in your computer.

https://nodejs.org/es/download/


## 🚀 Install the project

Install project dependencies with: npm i

## 🚀 Run the test: 
Open the terminal and run: npx cypress run (to run on headless)
npx cypress open (To run on the GUI)

