const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {

    
    setupNodeEvents(on, config) {
      // implement node event listeners here
      
    },
    "baseUrl": "https://the-internet.herokuapp.com",
    "defaultCommandTimeout": 80000,
    "pageLoadTimeout": 80000,
    "viewportHeight": 800,
    "viewportWidth": 1500
  },
});
