import {login} from "../fixtures/selectors";

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})
 
describe("Given I am on the login page", function () {
    beforeEach(function () {
      //cy.then(Cypress.session.clearCurrentSessionData)
      cy.visit('/')
 
    });
 
    it("LOGIN - I Should be able to login to my account", function () {
      cy.get(login.formAuthenticator).type('9096566944')
      cy.get(login.username).type("tomsmith")
      cy.get(login.password).type("SuperSecretPassword!")
      cy.get(login.loginBtn).click({force:true})
      cy.get(login.successMessage).contains(' You logged into a secure area! ')
    })
 
 
})
